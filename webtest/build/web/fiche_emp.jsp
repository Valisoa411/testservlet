<%-- 
    Document   : fiche_emp.jsp
    Created on : Nov 9, 2022, 2:11:06 PM
    Author     : Val
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Emp" %>
<%
    Emp emp = (Emp)request.getAttribute("emp");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1><%=emp.getNom()%></h1>
        <h2><%=emp.getId()%></h2>
    </body>
</html>
