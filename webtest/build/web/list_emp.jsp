<%-- 
    Document   : list_emp.jsp
    Created on : Nov 9, 2022, 2:11:42 PM
    Author     : Val
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Emp" %>
<%@page import="java.util.ArrayList" %>
<%
    ArrayList<Emp> ls = (ArrayList<Emp>)request.getAttribute("ls");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <% for(Emp e : ls){ %>
        <div>EMP<%=e.getId()%> <%=e.getNom()%></div>
    <% } %>
    </body>
</html>