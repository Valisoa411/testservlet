<%-- 
    Document   : list_dept.jsp
    Created on : Nov 9, 2022, 2:11:20 PM
    Author     : Val
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Dept" %>
<%@page import="java.util.ArrayList" %>
<%
    ArrayList<Dept> ls = (ArrayList<Dept>)request.getAttribute("ls");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <% for(Dept d : ls){ %>
        <div><%=d.getNom()%></div>
    <% } %>
    </body>
</html>
