<%-- 
    Document   : fiche_dept.jsp
    Created on : Nov 9, 2022, 2:10:41 PM
    Author     : Val
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.Dept" %>
<%
    Dept dept = (Dept)request.getAttribute("dept");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1><%=dept.getNom()%></h1>
    </body>
</html>
