/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.services;

import annotation.Path;
import java.util.ArrayList;
import model.Dept;
import util.ModelView;
import annotation.Secured;

/**
 *
 * @author Val
 */
public class DeptServices {
    @Path(url = "dept-list")
    public static ModelView getAll(){
        ModelView val = new ModelView();
        ArrayList<Dept> ls = new ArrayList<Dept>();
        ls.add(new Dept("RH"));
        ls.add(new Dept("Approv"));
        ls.add(new Dept("comm"));
        val.addAttribute("ls", ls);
        val.setTarget("list_dept.jsp");
        return val;
    }
}
