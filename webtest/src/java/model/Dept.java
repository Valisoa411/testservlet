/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Path;
import util.ModelView;
import annotation.Secured;

/**
 *
 * @author Val
 */
public class Dept {
    String nom;

    public Dept() {
    }
    
    public Dept(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @Path(url = "dept-get")
    public static ModelView getDept(){
        ModelView val = new ModelView();
        val.setTarget("fiche_dept.jsp");
        val.addAttribute("dept", new Dept("RH"));
        return val;
    }
}
