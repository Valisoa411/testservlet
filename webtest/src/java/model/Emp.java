/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Path;
import java.util.ArrayList;
import util.ModelView;
import annotation.Secured;

/**
 *
 * @author Val
 */
public class Emp {
    String nom;
    int id;

    public Emp() {
    }

    public Emp(String nom, int id) {
        this.nom = nom;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Path(url = "emp-list")
    public static ModelView getAll(){
        ModelView val = new ModelView();
        ArrayList<Emp> ls = new ArrayList<Emp>();
        ls.add(new Emp("Jean",1));
        ls.add(new Emp("Jeanne",2));
        ls.add(new Emp("Jeanno",3));
        val.addAttribute("ls", ls);
        val.setTarget("list_emp.jsp");
        return val;
    }
    
    @Path(url = "emp-get")
    public static ModelView getEmp(){
        ModelView val = new ModelView();
        val.setTarget("fiche_emp.jsp");
        val.addAttribute("emp", new Emp("Jean",1));
        return val;
    }
    
    @Path(url = "emp-create")
    public ModelView create() throws Exception {
        ModelView val = new ModelView();
        val.addAttribute("emp", this);
        val.setTarget("fiche_emp.jsp");
        return val;
    }
}
